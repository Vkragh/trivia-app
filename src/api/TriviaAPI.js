export const TriviaAPI = {
	fetchQuestions(options) {
		return fetch(
			`https://opentdb.com/api.php?amount=${options.numbers}&category=${options.category}&difficulty=${options.difficulty}&type=${options.type}&encode=url3986`
		)
			.then(response => response.json())
			.then(data => data.results);
	},
	fetchCategories() {
		return fetch('https://opentdb.com/api_category.php')
			.then(response => response.json())
			.then(data => data.trivia_categories);
	},
};
