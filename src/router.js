import VueRouter from "vue-router";
import Setup from "@/components/Setup/Setup.vue";
import Question from "@/components/Question/Question.vue";
import Result from "@/components/Result/Result.vue";

const routes = [
    {
        path: "/",
        alias: "/setup",
        name: "Setup",
        component: Setup,
    },
    {
        path: "/question",
        name: "Question",
        component: Question,
    },
    {
        path: "/result",
        name: "Result",
        component: Result,
    },
];

const router = new VueRouter({ routes });

export default router;
