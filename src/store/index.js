import Vue from "vue";
import Vuex from "vuex";
import shuffleArr from "@/util/shuffleArray";

import { TriviaAPI } from "@/api/TriviaAPI";

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        active: false,
        options: {},
        categories: [],
        questions: null,
        userAnswers: [],
        step: 0,
        score: 0,
        setupError: "",
        routerError: "",
        loading: false,
    },
    mutations: {
        setActive: (state, payload) => {
            state.active = payload;
        },
        setOptions: (state, payload) => {
            state.options = payload;
        },
        setCategories: (state, payload) => {
            state.categories = payload;
        },
        setQuestions: (state, payload) => {
            state.questions = payload.map((question) => {
                return {
                    ...question,
                    category: decodeURIComponent(question.category),
                    question: decodeURIComponent(question.question),
                    correct_answer: decodeURIComponent(
                        question.correct_answer.trim()
                    ),
                    incorrect_answers: question.incorrect_answers.map(
                        (answer) => {
                            return decodeURIComponent(answer.trim());
                        }
                    ),
                };
            });
        },
        resetUserAnswers: (state) => {
            state.userAnswers = [];
        },
        setUserAnswer: (state, payload) => {
            state.userAnswers.push(payload);
        },
        setStep: (state, payload) => {
            state.step = payload;
        },
        setScore: (state, payload) => {
            state.score = payload;
        },
        setSetupError: (state, payload) => {
            state.setupError = payload;
        },
        setRouterError: (state, payload) => {
            state.routerError = payload;
        },
        setLoading: (state, payload) => {
            state.loading = payload;
        },
    },
    getters: {
        getCurrentQuestion: (state) => {
            return state.questions !== null
                ? state.questions[state.step]
                : null;
        },
        getCurrentAnswers: (state) => {
            return shuffleArr([
                ...state.questions[state.step].incorrect_answers,
                state.questions[state.step].correct_answer,
            ]);
        },
    },
    actions: {
        async fetchQuestions({ commit, state }, router) {
            const { options } = state;
            try {
                commit("setLoading", true);
                const questions = await TriviaAPI.fetchQuestions(options);
                if (questions.length) {
                    commit("setQuestions", questions);
                    commit("setActive", true);
                    commit("setScore", 0);
                    commit("setStep", 0);
                    commit("setSetupError", "");
                    commit("resetUserAnswers");
                    router.push("/question");
                } else {
                    commit(
                        "setSetupError",
                        "There was no questions found with those options"
                    );
                }
            } catch (e) {
                commit("setError", e.message);
            } finally {
                commit("setLoading", false);
            }
        },
        async fetchCategories({ commit }) {
            try {
                const categories = await TriviaAPI.fetchCategories();
                commit("setCategories", categories);
            } catch (e) {
                commit("setError", e.message);
            }
        },
    },
});
