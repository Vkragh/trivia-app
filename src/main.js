import Vue from "vue";
import App from "./App.vue";
import store from "./store";
import VueRouter from "vue-router";
import router from "./router";
import "./assets/css/main.css";
import setTitle from "./util/setTitle";
import Fragment from "vue-fragment";

Vue.config.productionTip = false;

Vue.mixin(setTitle);
Vue.use(VueRouter);
Vue.use(Fragment.Plugin);

new Vue({
    router,
    store,
    render: (h) => h(App),
}).$mount("#app");
