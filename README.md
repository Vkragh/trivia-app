# Trivia Project

Trivia Quiz Developed with VueJS

Requirements for the Quiz:
Must have three routes

<h3>Start route</h3>
<ul>
<li>Your app should start on a “Start Screen.”</li>
<li>The user must be able to select the difficulty, number of questions and category.</li>
<li>The user must click a button or anywhere on the screen to start playing.</li>
</ul>
<h3>Question route</h3>
<ul>
<li>Once the game starts, the app must fetch the questions from the API setup in the previous step. The
app must ONLY display ONE question at a time.</li>
<li>If it is multiple choice, have 4 buttons with each answer as the text. If it is a True/False question, only
display 2 buttons, one for True and one for False.</li>
<li>Once a question is answered the app must move on to the next question.</li>
<li>When all the questions have been answered the user must be presented with the result screen.</li>
</ul>
<h3>Result route</h3>
<ul>
<li>The result screen must show all the questions along with the correct and user’s answers.</li>
<li>Display the total score.</li>
<li>Display a button to take the user back to the start screen or replay with different questions, but at the
current difficulty level and category.</li>
</ul>

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```
